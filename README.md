# TODO Backend

A series of implemetations of [Todo-Backend][todo-backend] REST API in **Scala** using different libraries.

- [x] Using `cask`: see `todobackend-cask` module
- [x] Using `http4s` with `cats-effect`: see `todobackend-http4s` module
- [x] Using `pekko-http`: see `todobackend-pekko-http` module
- [x] Using `zio-http` with `zio`: see `todobackend-zio-http` module

### Future implementations

- [ ] Using `play`: see `todobackend-play` module

  [todo-backend]: https://www.todobackend.com/
